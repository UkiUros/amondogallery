package rs.fourexample.amondogallery;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by urossimic on 4/28/18.
 **/

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.PhotoHolder> {

    private List<ImprintMedia> imprints;
    private Context context;

    public GalleryAdapter(Context context, List<ImprintMedia> imprints) {
        this.context = context;
        this.imprints = imprints;
    }

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhotoHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final PhotoHolder holder, int position) {
        if (imprints == null || imprints.isEmpty()) return;

        ImprintMedia media = imprints.get(position);

        Picasso.get()
                .load(Uri.fromFile(new File(media.getThumbnailPath())))
                .fit()
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imprints.size();
    }

    class PhotoHolder extends RecyclerView.ViewHolder {
        SquaredImageView imageView;

        PhotoHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.idImageView);
        }
    }
}
