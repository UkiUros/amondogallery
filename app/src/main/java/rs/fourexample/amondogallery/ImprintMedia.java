package rs.fourexample.amondogallery;

import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by urossimic on 4/30/18.
 **/

public class ImprintMedia implements Comparable<ImprintMedia> {

    private String absolutePath;
    private String thumbnailPath;
    private Date dateAdded;

    public ImprintMedia(String absolutePath, String thumbnailPath, Date dateAdded) {
        this.absolutePath = absolutePath;
        this.thumbnailPath = thumbnailPath;
        this.dateAdded = dateAdded;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Override
    public int compareTo(@NonNull ImprintMedia imprintMedia) {
        return getDateAdded().compareTo(imprintMedia.getDateAdded());
    }
}
