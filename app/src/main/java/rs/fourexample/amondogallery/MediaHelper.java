package rs.fourexample.amondogallery;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by urossimic on 4/30/18.
 **/

public class MediaHelper {

    private static final boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);

    public static List<ImprintMedia> getPhotoMedia(Context context,
                                                   Date startDate,
                                                   Date endDate,
                                                   double minLatitude,
                                                   double minLongitude,
                                                   double maxLatitude,
                                                   double maxLongitude) {

        String[] projection = {
                MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.DATE_ADDED,
                MediaStore.Images.Media.DATE_TAKEN,
                MediaStore.Images.Media.LONGITUDE,
                MediaStore.Images.Media.LATITUDE
        };

        Uri uri = isSDPresent ?
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                : MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        String selection = MediaStore.MediaColumns.DATE_ADDED + " >=? AND "
                + MediaStore.MediaColumns.DATE_ADDED + " <=? AND "
                + MediaStore.Images.Media.LATITUDE + " >=? AND "
                + MediaStore.Images.Media.LATITUDE + " <=? AND "
                + MediaStore.Images.Media.LONGITUDE + " >=? AND "
                + MediaStore.Images.Media.LONGITUDE + " <=? ";

        String[] selectionArgs = {
                "" + startDate.getTime() / 1000,
                "" + endDate.getTime() / 1000,
                "" + minLatitude,
                "" + maxLatitude,
                "" + minLongitude,
                "" + maxLongitude
        };

        Cursor cursor = context.getContentResolver().query(
                uri,
                projection,
                selection,
                selectionArgs,
                MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        int columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        int columnIndexThumbnail = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA);
        int columnIndexDateAdded = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED);

        List<ImprintMedia> listOfAllImages = new ArrayList<>();

        while (cursor.moveToNext()) {

            String absolutePathOfImage = cursor.getString(columnIndexData);
            String thumbnailPathOfImage = cursor.getString(columnIndexThumbnail);
            Date dateAdded = new Date(cursor.getLong(columnIndexDateAdded) * 1000); // converting seconds to milliseconds

            listOfAllImages.add(new ImprintMedia(absolutePathOfImage, thumbnailPathOfImage, dateAdded));

        }
        return listOfAllImages;
    }


    public static List<ImprintMedia> getVideoMedia(Context context,
                                                   Date startDate,
                                                   Date endDate,
                                                   double minLatitude,
                                                   double minLongitude,
                                                   double maxLatitude,
                                                   double maxLongitude) {
        String[] projection = {
                MediaStore.MediaColumns.DATA,
                MediaStore.Video.Media.DATE_ADDED,
                MediaStore.Video.Media.DATE_TAKEN,
                MediaStore.Video.Media.LONGITUDE,
                MediaStore.Video.Media.LATITUDE
        };

        Uri uri = isSDPresent ?
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                : MediaStore.Video.Media.INTERNAL_CONTENT_URI;

        String selection = MediaStore.MediaColumns.DATE_ADDED + " >=? AND "
                + MediaStore.MediaColumns.DATE_ADDED + " <=? AND "
                + MediaStore.Video.Media.LATITUDE + " >=? AND "
                + MediaStore.Video.Media.LATITUDE + " <=? AND "
                + MediaStore.Video.Media.LONGITUDE + " >=? AND "
                + MediaStore.Video.Media.LONGITUDE + " <=? ";

        String[] selectionArgs = {
                "" + startDate.getTime() / 1000,
                "" + endDate.getTime() / 1000,
                "" + minLatitude,
                "" + maxLatitude,
                "" + minLongitude,
                "" + maxLongitude
        };

        Cursor cursor = context.getContentResolver().query(
                uri,
                projection,
                selection,
                selectionArgs,
                MediaStore.Video.VideoColumns.DATE_TAKEN + " DESC");

        int columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        int columnIndexThumbnail = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);
        int columnIndexDateAdded = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED);

        List<ImprintMedia> listOfAllVideos = new ArrayList<>();

        while (cursor.moveToNext()) {
            String absolutePathOfVideo = cursor.getString(columnIndexData);
            String thumbnailPathOfVideo = cursor.getString(columnIndexThumbnail);
            Date dateAdded = new Date(cursor.getLong(columnIndexDateAdded) * 1000); // converting seconds to milliseconds

            listOfAllVideos.add(new ImprintMedia(absolutePathOfVideo, thumbnailPathOfVideo, dateAdded));

        }
        return listOfAllVideos;
    }
}
