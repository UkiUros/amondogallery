package rs.fourexample.amondogallery;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {

    private static final int RQ_EXTERNAL_STORAGE_PERMISSION = 101;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        recyclerView = findViewById(R.id.idRecyclerView);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        registerForContextMenu(recyclerView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    RQ_EXTERNAL_STORAGE_PERMISSION);

        } else {
            setupAdapter();
        }

    }

    private void setupAdapter() {
        recyclerView.setAdapter(new GalleryAdapter(this, getPhotosAndVideos()));
    }

    private List<ImprintMedia> getPhotosAndVideos() {
        Calendar calendarFrom = Calendar.getInstance();
        Calendar calendarTo = Calendar.getInstance(); // now

        calendarFrom.set(Calendar.MONTH, 4);
        calendarFrom.set(Calendar.DAY_OF_MONTH, 1);

        // my current location (and location of an photo in the gallery)
        double currentLat = 44.7605862;
        double currentLng = 19.2074671;

        Location minLocation = LocationHelper.getMinLocation(currentLat, currentLng);
        Location maxLocation = LocationHelper.getMaxLocation(currentLat, currentLng);

        List<ImprintMedia> photos = MediaHelper.getPhotoMedia(this,
                calendarFrom.getTime(), calendarTo.getTime(),
                minLocation.getLatitude(), minLocation.getLongitude(),
                maxLocation.getLatitude(), maxLocation.getLongitude());

        List<ImprintMedia> videos = MediaHelper.getVideoMedia(this,
                calendarFrom.getTime(), calendarTo.getTime(),
                minLocation.getLatitude(), minLocation.getLongitude(),
                maxLocation.getLatitude(), maxLocation.getLongitude());

        return mergePhotoVideo(photos, videos);
    }

    private List<ImprintMedia> mergePhotoVideo(List<ImprintMedia> photoList, List<ImprintMedia> videoList) {
        List<ImprintMedia> mergedList = new ArrayList<>();
        mergedList.addAll(photoList);
        mergedList.addAll(videoList);

        Collections.sort(mergedList, Collections.<ImprintMedia>reverseOrder());

        return mergedList;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RQ_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupAdapter();
                }
                break;
            }
        }
    }
}
