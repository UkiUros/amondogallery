package rs.fourexample.amondogallery;


import android.location.Location;

/**
 * Created by urossimic on 5/4/18.
 **/

public class LocationHelper {

    private static int DISTANCE = 500; // meters

    // 1m in degree = 0.0089 / 1000 = 0.0000089
    private static double METER_IN_DEGREE = 0.0000089;

    public static Location getMinLocation(double currLatitude, double currLongitude) {
        Location location = new Location("");

        double coef = DISTANCE * METER_IN_DEGREE;
        double minLatitude = currLatitude - coef;
        // pi / 180 = 0.018
        double minLongitude = currLongitude - coef / Math.cos(currLatitude * 0.018);
        location.setLatitude(minLatitude);
        location.setLongitude(minLongitude);

        return location;
    }

    public static Location getMaxLocation(double currLatitude, double currLongitude) {
        Location location = new Location("");

        double coef = DISTANCE * METER_IN_DEGREE;
        double minLatitude = currLatitude + coef;
        // pi / 180 = 0.018
        double minLongitude = currLongitude + coef / Math.cos(currLatitude * 0.018);
        location.setLatitude(minLatitude);
        location.setLongitude(minLongitude);

        return location;
    }
}
