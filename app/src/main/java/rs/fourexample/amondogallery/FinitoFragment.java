package rs.fourexample.amondogallery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FinitoFragment extends Fragment {

    public FinitoFragment() {
    }

    public static FinitoFragment newInstance() {
        // set arguments if needed
        return new FinitoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //get arguments if needed
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_finito, container, false);


        return v;
    }

}
